const webpack = require('webpack')
module.exports = {
    configureWebpack: {
        plugins: [
            new webpack.IgnorePlugin({
                resourceRegExp: /^\.\/locale$/,
                contextRegExp: /moment$/
            })
        ],
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: `
                @import "@/assets/variables.scss";
            `
            }
        }
    },
    chainWebpack: config => {
        config.module.rule('pdf')
            .test(/\.pdf$/)
            .use('file-loader').loader('file-loader')
    },
    devServer: {
        inline: true,
        port: 3001
    },
    publicPath: './',
    outputDir: 'public_html'
}