import Vue from 'vue'

import router from "./router/";
import store from './store/';

import App from './App.vue';


import '../node_modules/slick-carousel/slick/slick.css';
import "./assets/style.scss";
import './registerServiceWorker'


import VueYouTubeEmbed from 'vue-youtube-embed'
Vue.use(VueYouTubeEmbed)

var SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)

Vue.use(require('vue-moment'));

global.jQuery = require('jquery');
var $ = global.jQuery;
window.$ = $;

new Vue({
  el: '#app',
  router,
  axios,
  store,
  render: h => h(App)
})
