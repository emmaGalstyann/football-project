import Vue from "vue";
import Router from "vue-router";

const Home = () => import('@/views/Home.vue');
const About = () => import('@/views/About.vue');

const News = () => import('@/views/News.vue');
const NewsSimple = () => import('@/views/NewsSimple.vue');
const SingleNews = () => import('@/views/SingleNews.vue');

const NotFound = () => import('@/views/NotFound.vue');
const PrivacyPolicy = () => import('@/views/PrivacyPolicy.vue');

const Players = () => import('@/views/Players.vue');
const SinglePlayer = () => import('@/views/SinglePlayer.vue');

const Schedule = () => import('@/views/Schedule.vue');
const ScheduleResult = () => import('@/views/ScheduleResult.vue');

const Staff = () => import('@/views/Staff.vue');
const SingleStaff = () => import('@/views/SingleStaff.vue');
const Sponsors = () => import('@/views/Sponsors.vue');

const Standings = () => import('@/views/Standings.vue');

Vue.use(Router);

var router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: '/404',
      name: '404',
      component: NotFound,
    },
    {
      path: '*',
      redirect: '/404'
    },
    {
      path: "/news",
      name: "news",
      component: News
    },
    {
      path: '/news/video',
      name: 'news-simple',
      component: NewsSimple, props: { name: 'video' }
    },
    {
      path: '/news/photo',
      name: 'news-simple',
      component: NewsSimple, props: { name: 'photo' }
    },
    {
      path: "/standings",
      name: "standings",
      component: Standings
    },
    {
      path: "/schedule",
      name: "schedule",
      component: Schedule
    },
    {
      path: "/schedule-result/:id",
      name: "schedule-result",
      component: ScheduleResult
    },
    {
      path: "/players",
      name: "players",
      component: Players
    },
    {
      path: "/staff",
      name: "staff",
      component: Staff
    },
    {
      path: "/about",
      name: "about",
      component: About,
      //beforeEnter: AuthGuard
    },
    {
      path: "/sponsors",
      name: "sponsors",
      component: Sponsors,
    },
    {
      path: "/single-news/:id",
      name: "single-news",
      component: SingleNews,
    },
    {
      path: "/player/:id",
      name: 'single-player',
      component: SinglePlayer,
    },
    {
      path: "/staff/:id",
      name: 'single-staff',
      component: SingleStaff
    },
    {
      path: "/privacy-policy",
      name: 'privacy-policy',
      component: PrivacyPolicy, props: { name: 'privacy-policy' }
    },
    {
      path: "/legal-terms",
      name: 'legal-terms',
      component: PrivacyPolicy, props: { name: 'legal-terms' }
    }
  ],
});

router.beforeEach((to, from, next) => {
  if (from.name != to.name) {
    document.documentElement.classList.add('page-update');
    setTimeout(function () { next(); }, 1500)
  }
  else {
    next()
  }
})

router.afterEach((to, from, next) => {
  setTimeout(function () {
    document.documentElement.classList.remove('page-update');
  }, 2000)
})


/*router.beforeResolve((to, from, next) => {
  if (to.name) {
    NProgress.start()
    //store.commit("setLoading", true);
  }
  next()
})

router.afterEach((to, from) => {
  NProgress.done()
  //store.commit("setLoading", false);
  //document.querySelector(".home-hero").classList.add("in-viewport");
})
*/
export default router;