import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import news from './modules/news';
import mainNews from './modules/mainNews'
import players from './modules/players'
import teams from './modules/teams'
import matches from './modules/matches'
import sponsors from './modules/sponsors'
import seasons from './modules/seasons'
import about from './modules/aboutPage'
import home from './modules/homePage'

export default new Vuex.Store({
  //strict: true,
  modules: {
    news,
    mainNews,
    players,
    teams,
    matches,
    sponsors,
    seasons,
    about,
    home
  },
  state: {
    layout: 'default-layout',
    mobile: false,
    showModal: false,
    videoLink: '',
  },
  actions: {
    determineResolution({ commit }, payload) {
      commit("determineResolution", payload)
    },
  },
  mutations: {
    setLayout(state, payload) {
      state.layout = payload
    },
    showModal(state, payload) {
      state.showModal = payload
    },
    determineResolution(state, payload) {
      state.mobile = payload
    },
  },
  getters: {
    layout(state) {
      return state.layout
    },
    showModal(state) {
      return state.showModal;
    },
    videoLink(state) {
      return state.videoLink
    },
  },
})