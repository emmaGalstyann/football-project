export default {
    state: {
        mainNews: []
    },
    actions: {
        mainNews({ commit }, payload) {
            commit("mainNews", payload);
        },

    },
    mutations: {
        mainNews(state, payload) {
            state.mainNews = payload;
        },
    },
    getters: {
        mainNews(state) {
            return state.mainNews;
        },
    }
}