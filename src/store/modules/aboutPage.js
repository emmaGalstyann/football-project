export default {
    state: {
        aboutPage: {}
    },
    actions: {
        aboutPage({ commit }, payload) {
            commit("aboutPage", payload);
        },
    },
    mutations: {
        aboutPage(state, payload) {
            payload.map(el => {
                state.aboutPage = el;
            })

        },
    },
    getters: {
        aboutPage(state) {
            return state.aboutPage;
        },
    }
}