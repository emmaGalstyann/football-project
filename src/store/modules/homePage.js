export default {
    state: {
        homePage: {},
        slides: {}
    },
    actions: {
        homePage({ commit }, payload) {
            commit("homePage", payload);
        },
        slides({ commit }, payload) {
            commit('slides', payload)
        }
    },
    mutations: {
        homePage(state, payload) {
            payload.map(el => {
                state.homePage = el;
            })
        },
        slides(state, payload) {
            payload.map(el => {
                state.slides = el.slider.slides
            })
        }
    },
    getters: {
        homePage(state) {
            return state.homePage;
        },
        slides(state) {
            return state.homePage
        }
    }
}