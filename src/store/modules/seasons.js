export default {
    state: {
        seasons: []
    },
    actions: {
        setSeasons({ commit }, payload) {
            commit("setSeasons", payload);
        },
    },
    mutations: {
        setSeasons(state, payload) {
            state.seasons = payload;
        },
    },
    getters: {
        seasons(state) {
            return state.seasons;
        },
    }
}