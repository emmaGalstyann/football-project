export default {
    state: {
        matches: []
    },
    actions: {
        setMatch({ commit }, payload) {
            commit("setMatch", payload);
        },
    },
    mutations: {
        setMatch(state, payload) {
            state.matches = payload;
        },
    },
    getters: {
        match(state) {
            return state.matches;
        },
    }
}