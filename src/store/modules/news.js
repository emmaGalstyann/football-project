export default {
    state: {
        news: []
    },
    actions: {
        setNews({ commit }, payload) {
            commit("setNews", payload);
        },

    },
    mutations: {
        setNews(state, payload) {
            state.news = payload;
        },
    },
    getters: {
        news(state) {
            return state.news;
        },
    }
}